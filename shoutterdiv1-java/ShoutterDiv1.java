import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class ShoutterDiv1 {
	int HUB = 10000;

	public int count(String[] s1000, String[] s100, String[] s10, String[] s1,
			String[] t1000, String[] t100, String[] t10, String[] t1) {
		List<Integer> s = new ArrayList<Integer>();
		List<Integer> t = new ArrayList<Integer>();
		for (int i = 0; i < s1000.length; i++) {
			for (int j = 0; j < s1000[i].length(); j++) {
				s.add((s1000[i].charAt(j) - '0' * 1000)
						+ (s100[i].charAt(j) - '0' * 100)
						+ (s10[i].charAt(j) - '0' * 10)
						+ (s1[i].charAt(j) - '0'));
				
				t.add((t1000[i].charAt(j) - '0' * 1000)
						+ (t100[i].charAt(j) - '0' * 100)
						+ (t10[i].charAt(j) - '0' * 10)
						+ (t1[i].charAt(j) - '0'));
			}
		}
		
		TreeMap<Integer, List<Integer>> timeline = new TreeMap<Integer, List<Integer>>();
		for (int i = 0; i < s.size(); i++) {
			int head = s.get(i);
			int tail = t.get(i);
			
			List<Integer> list = timeline.get(head);
			if(list == null) {
				list = new ArrayList<Integer>();
				timeline.put(head, list);
			}
			list.add(i + 1);
			
			list = timeline.get(tail + 1);
			if(list == null) {
				list = new ArrayList<Integer>();
				timeline.put(tail + 1, list);
			}
			list.add(-i - 1);
		}
		
		Map<Integer, Map<Integer, Boolean>> graph = new HashMap<Integer, Map<Integer, Boolean>>();
		for(int i = 0; i < s.size(); i++) {
			Map<Integer, Boolean> adj = new HashMap<Integer, Boolean>();
			adj.put(i + 1 + HUB, true);
			graph.put(i + 1, adj);
			graph.put(i + 1 + HUB, new HashMap<Integer, Boolean>());
		}
		
		HashSet<Integer> currSet = new HashSet<Integer>();
		
		for(List<Integer> diffs: timeline.values()) {
			for(int rab : diffs) {
				if(rab < 0) currSet.remove(-rab);
			}
			for(int rab : diffs) {
				if(rab > 0) {
					for(int otherRab : currSet) {
						graph.get(otherRab + HUB).put(rab, false);
					}
					currSet.add(rab);
				}
			}
		}
		
		Map<Integer, Integer> union = new HashMap<Integer, Integer>();
		for(int i = 1; i <= s.size(); i++) {
			union.put(i, i);
			union.put(i + HUB, i + HUB);
		}
		
		for(int i = 1; i <= s.size(); i++) {
			for(Entry<Integer, Boolean> entry: graph.get(i + HUB).entrySet()) {
				int ownKey = union.get(i + HUB);
				int adjKey = union.get(entry.getKey());
				if(ownKey != adjKey) {
					for(int key : union.keySet()) {
						if(key == adjKey) {
							union.put(key, ownKey);
						}
					}
				}
			}
		}
		
		int count = 0;
		for(int i = 1; i <= s.size(); i++) {
			for(Entry<Integer, Boolean> entry: graph.get(i).entrySet()) {
				int ownKey = union.get(i);
				int adjKey = union.get(entry.getKey());
				if(ownKey != adjKey) {
					for(int key : union.keySet()) {
						if(key == adjKey) {
							union.put(key, ownKey);
						}
					}
					count++;
				}
			}
		}
		
		return count;
	}
}
