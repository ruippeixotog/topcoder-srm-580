import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class EelAndRabbitTest {

	protected EelAndRabbit solution;

	@Before
	public void setUp() {
		solution = new EelAndRabbit();
	}

	@Test
	public void testCase0() {
		int[] l = new int[] { 2, 4, 3, 2, 2, 1, 10 };
		int[] t = new int[] { 2, 6, 3, 7, 0, 2, 0 };

		int expected = 6;
		int actual = solution.getmax(l, t);

		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testCase1() {
		int[] l = new int[] { 1, 1, 1 };
		int[] t = new int[] { 2, 0, 4 };

		int expected = 2;
		int actual = solution.getmax(l, t);

		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testCase2() {
		int[] l = new int[] { 1 };
		int[] t = new int[] { 1 };

		int expected = 1;
		int actual = solution.getmax(l, t);

		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testCase3() {
		int[] l = new int[] { 8, 2, 1, 10, 8, 6, 3, 1, 2, 5 };
		int[] t = new int[] { 17, 27, 26, 11, 1, 27, 23, 12, 11, 13 };

		int expected = 7;
		int actual = solution.getmax(l, t);

		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testCaseCustom() {
		int[] l = { 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49,
				49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49,
				49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49,
				49, 49, 49 };
		int[] t = { 0, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550,
				600, 650, 700, 750, 800, 850, 900, 950, 1000, 1050, 1100, 1150,
				1200, 1250, 1300, 1350, 1400, 1450, 1500, 1550, 1600, 1650,
				1700, 1750, 1800, 1850, 1900, 1950, 2000, 2050, 2100, 2150,
				2200, 2250, 2300, 2350, 2400, 2450 };

		int expected = 2;
		int actual = solution.getmax(l, t);

		Assert.assertEquals(expected, actual);
	}
}
