
public class EelAndRabbit {

	public int getmax(int[] l, int[] t) {
		if(t.length == 1) {
			return 1;
		}
		int max = 0;
		for (int i = 0; i < t.length; i++) {
			for (int j = i + 1; j < t.length; j++) {
				int count = 0;
				for (int k = 0; k < t.length; k++) {
					if (t[k] <= t[i] && t[k] + l[k] >= t[i] || t[k] <= t[j]
							&& t[k] + l[k] >= t[j]) {
						count++;
					}
				}
				if (count > max) {
					max = count;
				}
			}
		}
		return max;
	}
}
